package ge.edu.btu.davit.soitashvili;

public class Dog {

    private String name;
    private String variety;
    private String sex;
    private int age;

    public Dog() {

    }

    public Dog(String name, String variety, String sex, int age) {
        this.name = name;
        this.variety = variety;
        this.sex = sex;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
