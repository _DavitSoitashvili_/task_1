package ge.edu.btu.davit.soitashvili;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    public static final String VIEW = "/registration.jsp";
    Dog dog = new Dog();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher view = req.getRequestDispatcher(VIEW);
        view.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        dog.setName(req.getParameter("name"));
        dog.setVariety(req.getParameter("variety"));
        dog.setSex(req.getParameter("sex"));
        dog.setAge((Integer.parseInt(req.getParameter("age"))));

        System.out.println("Name : " + dog.getName());
        System.out.println("Variety: " + dog.getVariety());
        System.out.println("Sex : " + dog.getSex());
        System.out.println("Age : " + dog.getAge());

    }

}
